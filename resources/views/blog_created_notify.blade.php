<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{$data['title']}}</title>
</head>
<body>
    <p>{{$data['description']}}</p>
    <a href="{{$data['url']}}">Click here to view blog.</a>
    <p>Thank You.</p>
</body>
</html>