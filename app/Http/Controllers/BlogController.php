<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;
use App\Http\Requests\StoreBlogRequest;
use App\Http\Requests\UpdateBlogRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\URL;
use Mail;


class BlogController extends Controller
{
    public function index(){
        $blogs = Blog::all();
        return $blogs;
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBlogRequest $request)
    {
        $blog = Blog::create($request->all());
        
        $url = $domain = URL::to('/');
        $data['url'] = $url;
        $data['email'] = "shivamsingh09882@gmail.com";
        $data['title'] = $blog->title;
        $data['description'] = $blog->description;

        Mail::send('blog_created_notify',['data' => $data], function($message) use ($data){
            $message->to($data['email'])->subject($data['title']);
        });
        return $blog;
    }

    /**
     * Display the specified resource.
     */
    public function show(Blog $blog)
    {
        return $blog;
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBlogRequest $request, Blog $blog)
    {
        $blog->update($request->all());
        return $blog;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Blog $blog)
    {
        $blog->delete();
        return response('',Response::HTTP_NO_CONTENT);
    }

    public function send_email(){
        $blog = Blog::latest()->get();
        $domain = URL::to('/');
        $url = $domain."/".$blog[0]['title'];
        $data['url'] = $url;
        $data['email'] = "shivamsingh09882@gmail.com";
        $data['title'] = $blog[0]['title'];
        $data['description'] = $blog[0]['description'];

        Mail::send('blog_created_notify',['data' => $data], function($message) use ($data){
            $message->to($data['email'])->subject($data['title']);
        });

        return response("Email Sent");
    }
}
