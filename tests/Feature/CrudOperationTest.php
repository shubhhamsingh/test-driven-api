<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Blog;

class CrudOperationTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_all_blog(){

        $blog = Blog::Factory()->create();

        $response = $this->getJson(route('blog.all'));

        $this->assertEquals($blog->title,$response[0]['title']);
        
    }

    public function test_create_blog(): void
    {
        $blog = Blog::factory()->create();

        $response = $this->postJson(route('Blog.store'),
        ['title' => $blog->title,'description' => $blog->description, 'keyword' => $blog->keyword])
        ->assertCreated();

        $this->assertEquals($blog->title,$response['title']);
        $this->assertDatabaseHas('blogs',['title' => $blog->title]);
    }

    public function test_fetch_single_blog()
    {
        $blog = Blog::factory()->create();

        $response = $this->getJson(route('blog.show',$blog->id))
                    ->assertOk();

        $this->assertEquals($blog->title,$response['title']);
    }


    public function test_update_blog(): void
    {
        $blog = Blog::Factory()->create();

        $response = $this->patchJson(route('blog.update',$blog->id),['title' => $blog->title,'description' => $blog->description])
        ->assertOk();

        $this->assertDatabaseHas('blogs',['title' => $blog->title,'description' => $blog->description]);
    }

    public function test_delete_blog()
    {
        $blog = Blog::Factory()->create();

        $response = $this->deleteJson(route('blog.delete',$blog->id))->assertNoContent();

        $this->assertDatabaseMissing('blogs',['title' => $blog->title]);
    }


}
