<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use  App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/blogs', [BlogController::class,'index'])->name('blog.all');
Route::post('/blog/add', [BlogController::class,'store'])->name('Blog.store');
Route::get('/blog/{blog}', [BlogController::class,'show'])->name('blog.show');
Route::patch('/blog-update/{blog}', [BlogController::class,'update'])->name('blog.update');
Route::delete('/blog/{blog}', [BlogController::class,'destroy'])->name('blog.delete');



